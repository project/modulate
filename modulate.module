<?php

$modulate_inc = variable_get('modulate_inc', conf_path() . '/modulate.inc');
if (file_exists($modulate_inc)) {
  include $modulate_inc;
}

/**
 * Implements hook_db_rewrite_sql().
 */
function modulate_db_rewrite_sql($query, $primary_table, $primary_field, $args) {
  global $theme_key;
  if ($primary_table == 'b' && $primary_field == 'bid') {
    $where = array();
    $conditions = function_exists('_modulate_block_conditions') ? _modulate_block_conditions() : array();
    foreach ($conditions as $condition) {
      $function = $condition['function_name'];
      if ($condition['theme'] == $theme_key && !(function_exists($function) && $function())) {
        $where[] = "NOT (b.module = '". $condition['module'] . "' AND b.delta = '" . $condition['delta'] . "') ";
      }
    }
    if ($where) {
      return array('where' => implode(' AND ', $where));
    }
  }
}

/**
 * Implements hook_views_api().
 */
function modulate_views_api() {
  return array(
    'api' => 3,
  );
}

/**
 * Implements hook_views_pre_view().
 */
function modulate_views_pre_view($view) {
  $modulate_views_data = function_exists('_modulate_views_data') ? _modulate_views_data() : array();
  foreach ($modulate_views_data as $view_data) {
    if ($view_data['view'] == $view->name) {
      $handler = $view->display[$view_data['display_id']]->handler;
      $option = $view_data['option'];
      $data = $handler->get_option($option);
      $data[$view_data['id']][$view_data['key']] = 'modulate';
      $handler->set_option($option, $data);
    }
  }
}

/**
 * Implementation of hook_filter()..
 *
 * Fires exported code or returns nothing and watchdogs.
 */
function modulate_filter($op, $delta = 0, $format = -1, $text = '') {
  switch ($op) {
    case 'list':
      return array(0 => t('Exported PHP evaluator'));
    case 'no cache':
      // No caching for the PHP evaluator.
      return $delta == 0;
    case 'description':
      return t('Executes a piece of PHP code. The usage of this filter should be restricted to administrators only!');
    case 'process':
      $function = '_modulate_filter_' . md5($text);
      if (function_exists($function)) {
        ob_start();
        $function();
        return ob_get_clean();
      }
      else {
        watchdog('modulate', '@text not found', array('@text' => $text));
        return '';
      }
    default:
      return $text;
  }
}

function _modulate_eval_is_disabled() {
  return in_array('eval', array_map('trim', explode(',', ini_get('disable_functions'))))
}

/**
 * Implements hook_form_content_field_edit_form_alter().
 *
 * Deny access to CCK allowed values PHP.
 */
function modulate_form_content_field_edit_form_alter(&$form) {
  if (_modulate_eval_is_disabled()) {
    foreach (array('field' => 'allowed_values', 'widget' => 'default_value') as $key1 => $key2) {
      if (isset($form[$key1][$key2 . '_fieldset']['advanced_options'][$key2 . '_php'])) {
        $element = &$form[$key1][$key2 . '_fieldset']['advanced_options'];
        if (count(element_children($element)) == 1) {
          // Kill the parent fieldset as well.
          $element['#access'] = FALSE;
        }
        else {
          // Only kill the PHP textbox.
          $element[$key2 . '_php']['#access'] = FALSE;
        }
      }
    }
  }
}

function modulate_menu_alter(&$items) {
  if (_modulate_eval_is_disabled()) {
    $paths = array(
      'admin/build/pages/import',
      'admin/build/panels/layouts/import',
      'admin/build/views/import',
      'admin/content/types/import',
      'admin/settings/skinr/import'
      'devel/php',
    );
    if (is_defined('FLAG_ADMIN_PATH')) {
      $paths[] = FLAG_ADMIN_PATH . '/import';
    }
    foreach ($paths as $path) {
      if (isset($items[$path])) {
        $items[$path]['access callback'] = FALSE;
      }
    }
  }
}