<?php

/**
 * Implement hook_drush_command().
 */
function modulate_drush_command() {
  $items = array();

  $items['modulate'] = array(
    'callback' => 'modulate_drush_generate',
    'description' => 'Generates the module.',
    'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_DATABASE,
  );
  return $items;
}

function _modulate_drush_create_function($function_name_parts, $php) {
  $function_name = '_modulate_' . implode('_', $function_name_parts);
  return "function $function_name() {?>". trim($php) . "<?php\n}";
}

function _modulate_drush_create_filter_function(&$functions, $code) {
  $hash = md5($code);
  $functions[$hash] = _modulate_drush_create_function(array('filter', $hash), $code);
}

function modulate_drush_generate() {
  if (!db_table_exists('modulate_blocks')) {
    db_query('CREATE TABLE {modulate_blocks} LIKE {blocks}');
  }
  if (!db_table_exists('modulate_permission')) {
    db_query('CREATE TABLE {modulate_permission} LIKE {permission}');
  }
  db_query('INSERT INTO {modulate_blocks} SELECT * FROM {blocks} WHERE visibility = 2 AND status = 1 ON DUPLICATE KEY
    UPDATE module = VALUES(module), delta = VALUES(delta), theme = VALUES(theme), status = VALUES(status), weight = VALUES(weight), region = VALUES(region), custom = VALUES(custom), throttle = VALUES(throttle), visibility = VALUES(visibility), pages = VALUES(pages), title = VALUES(title), cache = VALUES(cache)');
  db_query("UPDATE {blocks} SET visibility = 0, pages = '' WHERE visibility = 2 AND status = 1");

  // Remove PHP - related permissions.
  $php_permissions = array(
    'use PHP for block visibility',
    'Use PHP input for field settings (dangerous - grant with care)',
    'execute php code',
    'use php in custom pagers',
    'use PHP for fivestar target',
  );
  db_query("INSERT INTO {modulate_permission} SELECT * FROM {permission} WHERE perm LIKE '%%php%%' ON DUPLICATE KEY
    UPDATE rid = VALUES(rid), perm = VALUES(perm)");
  $result = db_query("SELECT pid, perm FROM {permission} WHERE perm LIKE '%%php%%'");
  while ($perm = db_fetch_object($result)) {
    $perm_array = array_map('trim', explode(',', $perm->perm));
    $update = FALSE;
    foreach ($php_permissions as $php_permission) {
      $php_index = array_search($php_permission, $perm_array);
      if ($php_index !==  FALSE) {
        unset($perm_array[$php_index]);
        $update = TRUE;
      }
    }
    if ($update) {
      db_query("UPDATE {permission} SET perm = '%s' WHERE pid = %d", implode(', ', $perm_array), $perm->pid);
    }
  }

  $result = db_query('SELECT theme, module, delta, pages FROM {modulate_blocks}');
  $block_conditions = array();
  while ($block = db_fetch_object($result)) {
    $function_name = array('block_visiblity', $block->theme, $block->module, str_replace('-', '_', $block->delta));
    $functions[] = _modulate_drush_create_function($function_name, $block->pages);
    $block_conditions[] = array(
      'theme' => $block->theme,
      'module' => $block->module,
      'delta' => $block->delta,
      'function_name' => $function_name,
    );
  }
  $php_format = db_result(db_query("SELECT format FROM {filter_formats} WHERE name = 'PHP code'"));
  db_query("UPDATE {filters} SET module = 'modulate' WHERE format = %d", $php_format);
  $modulate_views_data = array();
  if (module_exists('views')) {
    foreach (views_get_all_views() as $view) {
      $view->init_display();
      foreach ($view->display as $display_id => $display) {
        foreach (array('header', 'footer', 'empty', 'arguments') as $option) {
          if (!$display->handler->is_defaulted($option) && ($options = $display->handler->get_option($option))) {
            foreach ($options as $id => $data) {
              $function_name_parts = array('views', &$key, $view->name, $display_id, $option, $id);
              $modulate_data = array('view' => $view->name, 'display_id' => $display_id, 'option' => $option, 'id' => $id);
              if ($option == 'arguments') {
                foreach (array('default_argument' => '($view, $argument)', 'validate' => '($view, $handler, $argument)') as $type => $header) {
                  $key = $type . '_type';
                  if (isset($data[$key]) && $data[$key] == 'php') {
                    $modulate_views_data[] = $modulate_data + array('key' => $key);
                    $code = isset($data[$type . '_options']['code']) ? $data[$type . '_options']['code'] : $data[$type . '_php'];
                    $functions[] = 'function _modulate_'. implode('_', $function_name_parts) . $header .'{' . $code . '}';
                  }
                }
              }
              elseif ($php_format && isset($data['field']) && $data['field'] == 'area' && $data['table'] == 'views' && $data['format'] == $php_format) {
                $key = 'table';
                $modulate_views_data[] = $modulate_data + array('key' => $key);
                $functions[] = _modulate_drush_create_function($function_name_parts, $data['content']);
              }
            }
          }
        }
      }
    }
  }
  $result = db_query('SELECT teaser, body FROM {node_revisions} WHERE format = %d', $php_format);
  while ($node = db_fetch_object($result)) {
    _modulate_drush_create_filter_function($functions, $node->body);
    _modulate_drush_create_filter_function($functions, $node->teaser);
  }
  $result = db_query('SELECT body FROM {boxes} WHERE format = %d', $php_format);
  $block_bids = array();
  while ($block = db_fetch_object($result)) {
    _modulate_drush_create_filter_function($functions, $block->body);
  }
  if (module_exists('content')) {
    foreach (content_fields() as $field) {
      if ($field['type'] == 'text' && !empty($field['text_processing'])) {
        $db_info = content_database_info($field);
        $result = db_query('SELECT ' . $db_info['columns']['value']['column'] . ' AS value FROM {' . $db_info['table'] . '} WHERE ' . $db_info['columns']['format']['column'] . ' = %d', $php_format);
        while ($record = db_fetch_object($result)) {
          _modulate_drush_create_filter_function($functions, $record->value);
        }
      }
    }
  }
  if (module_exists('locale')) {
    $query = db_query('SELECT formula, language FROM {languages}');
    while ($language = db_fetch_object($result)) {
      $function_name = '_modulate_language_formula_' . $language->language;
      $functions[] = "function $function_name(\$n) {\nreturn intval(" . trim($language->formula) .  ");\n}";
    }
  }
  $block_conditions = var_export($block_conditions, TRUE);
  $modulate_views_data = var_export($modulate_views_data, TRUE);
  $functions = implode("\n\n", $functions);
  $file = <<<MODULE
<?php

function _modulate_block_conditions() {
  return $block_conditions;
}

function _modulate_views_data() {
  return $modulate_views_data;
}

$functions
MODULE;
  file_put_contents(variable_get('modulate_inc', conf_path() . '/modulate.inc'), $file);
}
