<?php

class modulate_plugin_argument_default extends views_plugin_argument_default {
  function get_argument() {
    $option = 'arguments';
    $display_id = $this->view->current_display;
    if ($this->view->display[$display_id]->handler->is_defaulted($option)) {
      $display_id = 'default';
    }
    $function_name = implode('_', array('_modulate_views', 'default_argument_type', $this->view->name, $display_id, $option, $this->argument->options['id']));
    ob_start();
    $result = $function_name($this->view, $this->argument);
    ob_end_clean();
    return $result;
  }
}
