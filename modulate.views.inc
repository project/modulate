<?php

function modulate_views_data() {
  $data['modulate']['table']['group'] = t('Global');
  $data['modulate']['table']['join'] = array(
    '#global' => array(),
  );
  $data['modulate']['area'] = array(
    'title' => t('Modulated text'),
    'help' => t('Exported PHP code for the area. Never add this manually, it will not display anything.'),
    'area' => array(
      'handler' => 'modulate_handler_area',
    ),
  );
  return $data;
}

function modulate_views_handlers() {
  return array(
    'handlers' => array(
      'modulate_handler_area' => array(
        'parent' => 'views_handler_area',
      ),
    ),
  );
}

function modulate_views_plugins() {
  return array(
    'argument default' => array(
      'modulate' => array(
        'no ui' => TRUE,
        'handler' => 'modulate_plugin_argument_default',
      ),
    ),
    'argument validator' => array(
      'modulate' => array(
        'no ui' => TRUE,
        'handler' => 'modulate_plugin_argument_validate',
      ),
    ),
  );
}
