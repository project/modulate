<?php

class modulate_plugin_argument_validate extends views_plugin_argument_validate {
  function validate_argument($argument) {
    $option = 'arguments';
    $display_id = $this->view->current_display;
    if ($this->view->display[$display_id]->handler->is_defaulted($option)) {
      $display_id = 'default';
    }
    $function_name = implode('_', array('_modulate_views', 'validate_type', $this->view->name, $display_id, $option, $this->argument->options['id']));
    ob_start();
    $result = $function_name($this->view, $this->argument, $argument);
    ob_end_clean();
    return $result;
  }
}
