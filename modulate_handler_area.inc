<?php

class modulate_handler_area extends views_handler_area {
  function render() {
    $option = $this->handler_type;
    $display_id = $this->view->current_display;
    if ($this->view->display[$display_id]->handler->is_defaulted($option)) {
      $display_id = 'default';
    }
    $function_name = implode('_', array('_modulate_views_table', $this->view->name, $display_id, $option, $this->options['id']));
    ob_start();
    $function_name();
    return ob_get_clean();
  }
}
